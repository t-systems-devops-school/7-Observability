## Homework

#homework #Observability

1. Install Prometheus in your namespace or locally on your computer (note that if you install Prometheus in your namespace in a cluster, you must do this with a minimum amount of resources since these are shared resources)
2. Configure the alert manager to trigger any rule at your discretion for your project (everything is fine, the target is unavailable, error 500 is repeated more than 3 times per minute etc)
3. Send a screenshot of what the rule worked to the chat